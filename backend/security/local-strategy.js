/**
 * Created by yacmed on 11/04/2016.
 */
'use strict'
var partsDirectory = __dirname+'./../../frontend/partials/';
var path = require('path');
var localStrategy = {
    ensureAuthenticated :  function(req,res,next){
        if (req.isAuthenticated()){
            next();
        } else {
            res.redirect('/login');
        }
    },
    ensureHasAdminPrivilege : function(req,res,next){
        var roles=req.user.roles;
        var isAuthorised=false;
        for(var i=0;i<roles.length;i++){
            if (roles[i].libelle==="administrateur"){
                isAuthorised=true;
            }
        }
        if (isAuthorised){
            next();
        } else {
            if (req.baseUrl.indexOf('api')>-1){
                res.json({
                    "error" : true,
                    "message" : "Vous n'\u00eates pas autoris\u00e9 "
                });
            } else {
                res.sendFile(path.join(partsDirectory+'privilege.html'));
            }
        }
    }
}
module.exports = localStrategy;
