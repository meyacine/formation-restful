/**
 * Created by yacmed on 14/03/2016.
 */
var expect = require('chai').expect;
var request = require('superagent');
describe('Sample web app',function(){

    var baseUrl = 'http://127.0.0.1:4000';
    describe('When requested at /Hello',function(){
        it('should say hello',function(done){
            request.get(baseUrl+'/Hello').end(function assert(err, res){
                expect(err).to.not.be.ok;
                expect(res).to.have.property('status',200);
                expect(res.text).to.equal('Hello world!');
                done();
            });
        });
    });
});
