/**
 * Created by yacmed on 11/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var articlesFacturationDao 		= require('./../dao/articles-facturation-dao');
var familleArticlesDao 		= require('./../dao/famille-articles-dao');
app.get('/', articlesFacturationDao.fetchArticles, function(request, result){
    if (!result.body.error) {
        result.json(result.body);
    }
});
app.get('/history',function(request, result){
    articlesFacturationDao.fetchAllArticles(function(resultat){
        result.json(resultat);
    });
});
module.exports = app;
