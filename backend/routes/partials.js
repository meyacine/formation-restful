/**
 * Created by yacmed on 11/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var partsDirectory = __dirname+'./../../frontend/partials/';
var path = require('path');

app.get('/charges', function(request,result){
    result.sendFile(path.join(partsDirectory+'charges.html'));
}),
app.get('/ressources', function(request,result){
    result.sendFile(path.join(partsDirectory+'ressources.html'));
}),
app.get('/modes-paiements', function(request,result){
    result.sendFile(path.join(partsDirectory+'modes-paiements.html'));
}),
app.get('/modes-gestion-ressources', function(request,result){
    result.sendFile(path.join(partsDirectory+'modes-gestion-ressources.html'));
}),
app.get('/type-tiers-payeur', function(request,result){
    result.sendFile(path.join(partsDirectory+'type-tiers-payeur.html'));
}),
app.get('/familles-articles', function(request,result){
    result.sendFile(path.join(partsDirectory+'familles-articles.html'));
}),
app.get('/articles-facturation', function(request,result){
    result.sendFile(path.join(partsDirectory+'articles-facturation.html'));
});
module.exports = app;

