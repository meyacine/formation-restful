/**
 * Created by yacmed on 11/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var typeTiersPayeurApi 		= require('./../dao/type-tiers-dao');
app.get('/',function(request, result){
    typeTiersPayeurApi.fetchTypesTiersPayeurs(function(resultat){
        result.json(resultat);
    });
});
app.get('/history',function(request, result){
    typeTiersPayeurApi.fetchAllTypesTiersPayeurs(function(resultat){
        result.json(resultat);
    });
});
app.get('/:id',function(request, result){
    typeTiersPayeurApi.fetchTypeTiersPayeur(request, function(resultat){
        result.json(resultat);
    });
});
app.post('/',function(request, result){
    typeTiersPayeurApi.addTypeTiersPayeur(request, function(resultat){
        result.json(resultat);
    });
});
app.delete('/:id',function(request, result){
    typeTiersPayeurApi.deleteTypeTiersPayeurs(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/',function(request, result){
    request.params.id = request.body.id; // Par convention on appel l'url DELETE /:id donc je remet l'id dans l'entete pour ne pas reecrire la m�thoe et violer les best practices
    typeTiersPayeurApi.deleteTypeTiersPayeurs(request,function(resultat){
        if (resultat.datas){
            // resultat est un json qui contient dans datas l'ID de la ligne desactiv�e
            request.body.ancienIdFk=resultat.datas;
            // Je renseigne l'id transmis dans le body de la request � null pour �viter la DUPLICATE_KEY_ENTRY
            request.body.id=null;
            // Sur le frontend j'envoie un editionMode (Angular) pour switcher entre l'�dition et la lecture seule. Ce param est a enlever
            delete request.body.editionMode;
            typeTiersPayeurApi.addTypeTiersPayeur(request,function(resultat){
                resultat.message="Type de tiers payeurs remplac\u00e9 par le type N\u26ac "+resultat.datas;
                result.json(resultat);
            });
        } else {
            result.json(resultat);
        }

    });
});
module.exports = app;
