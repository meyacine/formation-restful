/**
 * Created by yacmed on 08/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var chargesDao 		= require('./../dao/charges-dao');
app.get('/',function(request, result){
    chargesDao.fetchCharges(function(resultat){
        result.json(resultat);
    });
});
app.get('/history',function(request, result){
    chargesDao.fetchAllCharges(function(resultat){
        result.json(resultat);
    });
});
app.get('/:id',function(request, result){
    chargesDao.fetchCharge(request, function(resultat){
        result.json(resultat);
    });
});
app.post('/',function(request, result){
    chargesDao.addCharge(request, function(resultat){
        result.json(resultat);
    });
});
app.delete('/:id',function(request, result){
    chargesDao.deleteCharge(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/',function(request, result){
    request.params.id = request.body.id; // Par convention on appel l'url DELETE /:id donc je remet l'id dans l'entete pour ne pas reecrire la m�thoe et violer les best practices
    chargesDao.deleteCharge(request, function(resultat){
        if (resultat.datas){
            // resultat est un json qui contient dans datas l'ID de la ligne desactiv�e
            request.body.ancienIdFk=resultat.datas;
            // Je renseigne l'id transmis dans le body de la request � null pour �viter la DUPLICATE_KEY_ENTRY
            request.body.id=null;
            // Sur le frontend j'envoie un editionMode (Angular) pour switcher entre l'�dition et la lecture seule. Ce param est a enlever
            delete request.body.editionMode;
            chargesDao.addCharge(request,function(resultat){
                resultat.message="charge remplac\u00e9 par la charge N\u26ac "+resultat.datas;
                result.json(resultat);
            });
        } else {
            result.json(resultat);
        }
    });
});
module.exports = app;