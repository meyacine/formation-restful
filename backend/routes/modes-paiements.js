/**
 * Created by yacmed on 08/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var modesPaiementApi 		= require('./../dao/mode-paiement-dao');
app.get('/',function(request, result){
    modesPaiementApi.fetchModesPaiements(function(resultat){
        result.json(resultat);
    });
});
app.get('/history',function(request, result){
    modesPaiementApi.fetchAllModesPaiements(function(resultat){
        result.json(resultat);
    });
});
app.get('/:id',function(request, result){
    modesPaiementApi.fetchModePaiement(request, function(resultat){
        result.json(resultat);
    });
});
app.post('/',function(request, result){
    modesPaiementApi.addModesPaiement(request, function(resultat){
        result.json(resultat);
    });
});
app.delete('/:id',function(request, result){
    modesPaiementApi.deleteModesPaiement(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/',function(request, result){
    request.params.id = request.body.id; // Par convention on appel l'url DELETE /:id donc je remet l'id dans l'entete pour ne pas reecrire la m�thoe et violer les best practices
    modesPaiementApi.deleteModesPaiement(request, function(resultat){
        if (resultat.datas){
            // resultat est un json qui contient dans datas l'ID de la ligne desactiv�e
            request.body.ancienIdFk=resultat.datas;
            // Je renseigne l'id transmis dans le body de la request � null pour �viter la DUPLICATE_KEY_ENTRY
            request.body.id=null;
            // Sur le frontend j'envoie un editionMode (Angular) pour switcher entre l'�dition et la lecture seule. Ce param est a enlever
            delete request.body.editionMode;
            modesPaiementApi.addModesPaiement(request,function(resultat){
                resultat.message="Mode de paiement remplac\u00e9 par le mode N\u26ac "+resultat.datas;
                result.json(resultat);
            });
        } else {
            result.json(resultat);
        }

    });
});
module.exports = app;