/**
 * Created by yacmed on 11/04/2016.
 */
'use strict'
var express = require('express');
var app  = express.Router();
var rolesDao 		= require('./../dao/roles-dao');
app.get('/', function(request, result){
    rolesDao.fetchRoles(function(resultat){
        result.json(resultat);
    });
});
app.get('/:id',  function(request, result){
    rolesDao.fetchRoleById(request, function(resultat){
        result.json(resultat);
    });
});
app.post('/',  function(request, result){
    rolesDao.addRole(request, function(resultat){
        result.json(resultat);
    });
});
app.delete('/:id',  function(request, result){
    rolesDao.deleteRole(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/',  function(request, result){
    rolesDao.updateRole(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/',  function(request, result){
    rolesDao.updateRole(request, function(resultat){
        result.json(resultat);
    });
});
module.exports = app;
