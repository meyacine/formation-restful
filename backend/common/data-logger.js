/**
 * Created by yacmed on 08/03/2016.
 */
'use strict'
var databaseConnection = require('./dbc');
var mysql = require('mysql');
var connexionBDD =  databaseConnection.getConnection();
/**
 * Save query on history table
 * @param query
 */
exports.log = function (query) {
    connexionBDD.query("INSERT INTO history (query) VALUES (?)",query,function(err){
        if(err) {
            console.error("Error when logging "+err.code);
        }
    });
};