-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 24 Mars 2016 à 17:16
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `aide-sociale`
--

-- --------------------------------------------------------

--
-- Structure de la table `asoc_mode_gestion_ressources`
--

CREATE TABLE IF NOT EXISTS `asoc_mode_gestion_ressources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(200) NOT NULL,
  `taux` decimal(5,2) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `dateCreation` datetime NOT NULL,
  `profilCreation` varchar(20) NOT NULL,
  `profilSuppression` varchar(20) DEFAULT NULL,
  `dateSuppression` date DEFAULT NULL,
  `ancienIdFk` int(11) DEFAULT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `asoc_mode_gestion_ressources`
--

INSERT INTO `asoc_mode_gestion_ressources` (`id`, `libelle`, `taux`, `description`, `dateCreation`, `profilCreation`, `profilSuppression`, `dateSuppression`, `ancienIdFk`, `flag`) VALUES
(14, 'Le resident ne verse pas ses ressources', '0.00', 'aucune description', '2016-03-15 00:00:00', 'YACINE', NULL, NULL, NULL, 0),
(15, 'Le resident verse ses ressources', '100.00', '', '2016-03-15 00:00:00', 'YACINE', NULL, NULL, 14, 0),
(16, 'Le resident verse 90', '90.00', '90% des ressouces', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(17, 'Le résident verse 90% des ses ressources', '90.00', '90% des ressouces, on laisse l''ADP', '0000-00-00 00:00:00', '', NULL, NULL, 16, 0),
(18, 'Le resident verse ses ressources à 100%', '100.00', 'On doit verser l''ADP au résident', '2016-03-14 23:00:00', 'YACINE', NULL, NULL, 15, 0),
(19, 'On n''encaise pas les ressources', '0.00', 'OK', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(20, 'On n''encaise pas les ressources', '0.00', 'OK', '0000-00-00 00:00:00', '', NULL, NULL, 19, 0),
(21, 'Le résident verse 90% des ses ressources', '90.00', '90% des ressouces, on laisse l''ADP', '0000-00-00 00:00:00', '', NULL, NULL, 17, 1),
(22, 'ok', '20.00', 'ddd', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(23, 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', '10.00', NULL, '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(24, 'On n''encaise pas les ressources', '0.00', 'OK', '0000-00-00 00:00:00', '', NULL, NULL, 20, 0),
(25, 'OK', '100.00', 'okok', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 1),
(26, 'ok', '20.00', 'ddd', '0000-00-00 00:00:00', '', NULL, NULL, 22, 0),
(27, 'On n''encaise que dalle', '0.00', 'OK', '0000-00-00 00:00:00', '', NULL, NULL, 24, 1);

-- --------------------------------------------------------

--
-- Structure de la table `asoc_mode_paiement`
--

CREATE TABLE IF NOT EXISTS `asoc_mode_paiement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(200) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `dateCreation` datetime NOT NULL,
  `profilCreation` varchar(20) NOT NULL,
  `profilSuppression` varchar(20) DEFAULT NULL,
  `dateSuppression` date DEFAULT NULL,
  `ancienIdFk` int(11) DEFAULT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `asoc_mode_paiement`
--

INSERT INTO `asoc_mode_paiement` (`id`, `libelle`, `description`, `dateCreation`, `profilCreation`, `profilSuppression`, `dateSuppression`, `ancienIdFk`, `flag`) VALUES
(1, 'Le tiers paye la totalité au résident', 'Paye la totalité', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(2, 'ok', 'ko', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(3, 'ko', 'ko', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(4, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, 3, 0),
(5, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(6, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, 5, 0),
(7, 'kok', 'ko', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(8, 'kok', 'ko', '0000-00-00 00:00:00', '', NULL, NULL, 7, 0),
(9, 'kok', 'okokokokok', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(10, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(11, 'ko', 'ko', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(12, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(13, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, 12, 0),
(14, 'ok', 'ok', '0000-00-00 00:00:00', '', NULL, NULL, 13, 0),
(15, 'Le tiers paye la totalité', 'TotFact', '0000-00-00 00:00:00', '', NULL, NULL, NULL, 0),
(16, 'Le tiers paye la totalité', 'Totalité de la facture', '0000-00-00 00:00:00', '', NULL, NULL, 15, 0);

-- --------------------------------------------------------

--
-- Structure de la table `charges`
--

CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) NOT NULL,
  `profilCreation` varchar(10) NOT NULL,
  `dateCreation` date NOT NULL,
  `flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `charges`
--

INSERT INTO `charges` (`id`, `libelle`, `profilCreation`, `dateCreation`, `flag`) VALUES
(7, 'nada', '', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(1000) NOT NULL,
  `at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=193 ;
--
-- Structure de la table `ressources`
--

CREATE TABLE IF NOT EXISTS `ressources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) NOT NULL,
  `taux` decimal(5,2) NOT NULL,
  `profilCreation` varchar(10) NOT NULL,
  `dateCreation` date NOT NULL,
  `flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `ressources`
--

INSERT INTO `ressources` (`id`, `libelle`, `taux`, `profilCreation`, `dateCreation`, `flag`) VALUES
(22, 'KLESIA', '90.00', '', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `libelle`, `flag`) VALUES
(1, 'administrateur', 1),
(2, 'Tiers', 1),
(3, 'Comptable', 1),
(4, 'Contentieux', 1),
(5, 'Tarification', 1),
(6, 'Qualite', 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` char(60) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `flag`) VALUES
(2, 'yacine', 'clearPwd', 1),
(3, 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userIdFk` int(11) NOT NULL,
  `roleIdFk` int(11) NOT NULL,
  `profilCreation` int(11) NOT NULL,
  `dateCreation` date NOT NULL,
  `profilSuppression` int(11) DEFAULT NULL,
  `dateSuppression` date DEFAULT NULL,
  `ancienIdFk` int(11) DEFAULT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `user_roles`
--

INSERT INTO `user_roles` (`id`, `userIdFk`, `roleIdFk`, `profilCreation`, `dateCreation`, `profilSuppression`, `dateSuppression`, `ancienIdFk`, `flag`) VALUES
(1, 3, 1, 0, '0000-00-00', NULL, NULL, NULL, 1),
(2, 3, 5, 0, '0000-00-00', NULL, NULL, NULL, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
