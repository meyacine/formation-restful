/**
 * Created by yacmed on 18/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
var UsersDao = {
    /**
     * Recup�re la liste des users et fait appel � la callback qui prend en param le r�sultat {error, message, datas}
     * @param callback
     */
    fetchUsers: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT id, username, flag FROM users WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des utilisateurs "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    /**
     * Recup�re un utilisateur par son #ID, prend en param�tres la requete HTTP, et la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    fetchUserById: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT id, username, flag FROM users WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la r\u00e9cup\u00e9ration de l'utilisateur "+err.code
                };
                console.error(err);
            } else {
                if (rows.length>0){
                    answer = {
                        "error" : false,
                        "message" : "Succes",
                        "datas" : rows
                    };
                } else {
                    answer = {
                        "error":true,
                        "message" : "Aucun utilisateur ne correspond \u00e0 l'id : "+request.params.id
                    }
                }

            }
            callback(answer);
        });
    },
    /**
     * Ajoute l'utilisateur pass�e dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    addUser: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO users SET ?";
        connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'ajout de l'utilisateur "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Utilisateur Ajout\u00e9 !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
    },
    /**
     * Supprime l'utilisateur pass�e dans la requete HTTP #ID, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    deleteUser: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE users SET flag=0 WHERE flag=1 AND ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la d\u00e9sactivation du compte utilisateur"
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "utilisateur supprim\u00e9 "+request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun utilisateur ne correspond \u00e8 cet id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    },
    /**
     * Met � jour la user pass�e dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    updateUser: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE ?? SET ?? = ? WHERE flag = 1 AND ?? = ?";
        var binding = ["users","password",request.body.password,"id",request.body.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la mise \u00e0 jour du mot de passe"+err.code
                };
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "Mot de passe mis \u00e0 jours "+request.body.id,
                        "data":request.body.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun utilisateur ne correspond \u00e0 l'id : "+request.body.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    },
    login : function(username,password, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT id, username FROM users WHERE flag=1 AND username=? AND password=?";
        var binding = [username, password];
        query = mysql.format(query,binding);
        connexionBDD.query(query, function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'authentification "+err.code
                };
                console.error(err);
            } else {
                if (rows.length===1){
                    answer = {
                        "error" : false,
                        "message" : "Utilisateur Authentifi\u00e9 !",
                        "datas":rows
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Nom d'utilisateur ou mot de passe \u00e9rron\u00e9"
                    };
                }

            }
            callback(answer);
        });
    }
}
exports.usersDao = UsersDao;