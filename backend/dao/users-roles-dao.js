/**
 * Created by yacmed on 18/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
var usersApi = require('./users-dao');
var rolesApi = require('./roles-dao');
var UsersRolesDao = {
    /**
     * Recup�re la liste des roles d'un utilisateur par son ID et fait appel � la callback qui prend en param le r�sultat {error, message, datas}
     * @param callback
     */
    fetchUserRolesByUserId: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        /**
         * TODO : Yacine Quand on aura plus de volum�trie il faudra comparer les trois m�thodes
         * @type {string}
         */
        // var query = "SELECT * FROM roles WHERE flag=1 AND id IN (SELECT roleIdFk FROM user_roles WHERE userIdFK=? AND flag =1)";
        // var query = "SELECT roles.* FROM roles, user_roles WHERE roles.flag=1 AND user_roles.flag=1 AND roles.id = roleIdFK AND userIdFk=?"
        var query = "SELECT roles.* FROM roles JOIN (user_roles) ON (user_roles.roleIdFk=roles.id) WHERE roles.flag=1 AND user_roles.flag=1 AND user_roles.userIdFk =?";
        var binding = [request.id];
        query = mysql.format(query,binding);
        console.log(query);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des roles de l'utilisateur id "+request.id + " Code erreur : "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : {
                        "id": request.id,
                        "username": request.username,
                        "avatar": request.avatar,
                        "roles" : rows
                    }
                };
            }
            callback(answer);
        });
    }
}
exports.userRolesDao = UsersRolesDao;