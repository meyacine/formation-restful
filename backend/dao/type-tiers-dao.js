/**
 * Created by yacmed on 15/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
var TypeTiersPayeurDao = {
    fetchAllTypesTiersPayeurs: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_type_tiers ORDER BY dateCreation DESC";
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des types de tiers payeurs "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    fetchTypesTiersPayeurs: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_type_tiers WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des types de tiers payeurs "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    fetchTypeTiersPayeur: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_type_tiers WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur de recuperation du type de tiers payeurs "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    addTypeTiersPayeur: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO asoc_type_tiers SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        var qey = connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'ajout du type de tiers payeur "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Type de tiers payeur ajout\u00e9 !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
        console.log(qey.sql);
    },
    deleteTypeTiersPayeurs: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE asoc_type_tiers SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur de la desactivation du type de tiers payeur "+err.code
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "Type de tiers payeur supprime "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun type de tiers payeur n'a �t� trouv� avec l'id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    }
}
module.exports = TypeTiersPayeurDao;