/**
 * Created by yacmed on 14/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
var FamillesArticlesDao = {
    /**
     * Methode utilis�e souvent pour r�cup�rer l'historique pour les champs inactifs
     * @param callback
     */
    fetchAllFamillesArticles: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_famille_articles ORDER BY dateCreation DESC";
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation de l'historique des familles des articles "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    fetchFamillesArticles: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_famille_articles WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des familles des articles "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    fetchArticlesFamillies: function(request, result, next){
        var connexionBDD =  databaseConnection.getConnection();
        var resultat = [];
        result.body.datas.forEach(function(article) {
            request.params.id=article.familleArticlesId;
        });
        next();
    },
    fetchFamilleArticles: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_famille_articles WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation de la famille d'articles "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    addFamilleArticles: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO asoc_famille_articles SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        var qey = connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'ajout de la famille d'articles "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Famille d'article ajout\u00e9e !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
        console.log(qey.sql);
    },
    deleteFamilleArticles: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE asoc_famille_articles SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'execution de la requ\u00eate "+err.code
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "Famille d'articles supprim\u00e9e "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucune famille d'articles n'a \u00e9t\u00e9 trouv\u00e9e  "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    }
}
module.exports = FamillesArticlesDao;
