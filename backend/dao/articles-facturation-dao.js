/**
 * Created by yacmed on 14/03/2016.
 */

'use strict'
var databaseConnection = require('./../common/dbc');
var dataLogger = require('./../common/data-logger');
var mysql = require('mysql');
var ArticleFacturationDao = {
    fetchArticles : function (request, result, next) {
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_articles_facturation WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des articles de facturation "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            result.body = answer;
            next();
        });
    },
    fetchAllArticles : function (callback) {
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_articles_facturation ORDER BY dateCreation DESC";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des articles pour l'historique "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    }
    ,fetchArticle : function(request,callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_articles_facturation WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "erreur de la r\u00e9cup\u00e9ration de l'article "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    }
    ,addArticle : function(request,callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO asoc_articles_facturation SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'ajout de l'article de facturation"+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Article ajout\u00e9e !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
    }
    ,deleteArticle : function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE asoc_articles_facturation SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query"
                };
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "charge supprim\u00e9e "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucune charge trouv\u00e9e avec l'id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLogger.log(parsedQuery);
    }
};

module.exports = ArticleFacturationDao;
