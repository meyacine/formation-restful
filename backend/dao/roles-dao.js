/**
 * Created by yacmed on 18/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
var RolesDao = {
    /**
     * Recup�re la liste des roles et fait appel � la callback qui prend en param le r�sultat {error, message, datas}
     * @param callback
     */
    fetchRoles: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM roles WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des roles "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    /**
     * Recup�re un roles par son #ID, prend en param�tres la requete HTTP, et la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    fetchRoleById: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM roles WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la r\u00e9cup\u00e9ration du r\u00f4le "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    /**
     * Ajoute le r�le pass�e dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    addRole: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO roles SET ?";
        connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de l'ajout du r\u00f4le "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "R\u00f4le ajout\u00e9 !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
    },
    /**
     * Supprime le r�le pass� dans la requete HTTP #ID, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    deleteRole: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE roles SET flag=0 WHERE flag=1 AND ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la d\u00e9sactivation du r\u00f4le"
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "r\u00f4le supprim\u00e9 "+request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun r\u00f4le ne correspond \u00e8 cet id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    },
    /**
     * Met � jour la r�le pass� dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    updateRole: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE ?? SET ?? = ? WHERE flag = 1 AND ?? = ?";
        var binding = ["roles","libelle",request.body.libelle,"id",request.body.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la mise \u00e0 jour du r\u00f4le "+err.code
                };
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "r\u00f4le  mis \u00e0 jours "+request.body.id,
                        "data":request.body.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun r\u00f4le ne correspond \u00e0 l'id : "+request.body.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    }
}
exports.rolesDao = RolesDao;