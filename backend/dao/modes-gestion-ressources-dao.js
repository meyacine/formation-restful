/**
 * Created by yacmed on 15/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
/**
 * Module de gestion (CRUD) des modes de gestions des ressources de residents
 * @type {{}}
 */
var ModeGestionRessourcesDao = {
    /**
     * Methode utilis�e souvent pour r�cup�rer l'historique pour les champs inactifs
     * @param callback
     */
    fetchAllModesGestionRessources: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_gestion_ressources ORDER BY dateCreation DESC";
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation de l'historique des modes de gestion de ressources "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    fetchModesGestionRessources: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_gestion_ressources WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des modes de gestion de ressources "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    fetchModeGestionRessources: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_gestion_ressources WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing fetchModeGestionRessources query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    addModesGestionRessources: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO asoc_mode_gestion_ressources SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        var qey = connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Mode de gestion Added !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
        console.log(qey.sql);
    },
    deleteModesGestionRessources: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE asoc_mode_gestion_ressources SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "Mode de gestion de ressources supprime "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun mode de foncctionnement n'a �t� trouv�  "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    }
}
module.exports = ModeGestionRessourcesDao;