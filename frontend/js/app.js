/**
 * Created by SEILI on 05/03/2016.
 */
'use strict'
var app = angular.module('MainApp', ['ui.router']);
app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',function($stateProvider, $urlRouterProvider,$httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        if ($rootScope.activeCalls == undefined) {
            $rootScope.activeCalls = 0;
        }
        return {
            request: function (config) {
                $rootScope.activeCalls += 1;
                return config;
            },
            requestError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            },
            response: function (response) {
                $rootScope.activeCalls -= 1;
                return response;
            },
            responseError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            }
        };
    });
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/home");
    // Now set up the states
    $stateProvider.state('home', {
            url: "/home",
            templateUrl: "partials/home.html"
        })
        .state('params', {
            url : '/params',
            templateUrl: "partials/params.html",
            controller : 'AppController'
        })
        .state('params.ressources', {
            url:"/ressources",
            templateUrl: "partials/ressources",
            controller : 'RessourcesCtrl'
        })
        .state('params.charges', {
            url:"/charges",
            templateUrl: "partials/charges",
            controller : 'ChargesCtrl'
        })
        .state('params.modesPaiements', {
            url:"/modesPaiements",
            templateUrl: "partials/modes-paiements",
            controller : 'ModesPaiementsCtrl'
        })
        .state('params.modesGestionRessources', {
            url:"/modesGestionRessources",
            templateUrl: "partials/modes-gestion-ressources",
            controller : 'ModesGestionRessourcesCtrl'
        })
        .state('params.typesTiersPayeurs', {
            url:"/typesTiersPayeurs",
            templateUrl: "partials/type-tiers-payeur",
            controller : 'TypeTiersPayeurCtrl'
        })
        .state('params.famillesArticles', {
            url:"/famillesArticles",
            templateUrl: "partials/familles-articles",
            controller : 'FamillesArticlesCtrl'
        })
        .state('params.articlesFacturation', {
            url:"/articlesFacturation",
            templateUrl: "partials/articles-facturation",
            controller : 'ArticlesFacturationCtrl'
        })
        .state('about', {
            url: "/about",
            templateUrl: "partials/about.html",
            controller : function(){
                console.log('about');
            }
        });
}]);
