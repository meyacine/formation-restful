/**
 * Created by yacmed on 08/03/2016.
 */
app.factory('RessourcesSvc',function($http){
    const URL ='api/ressources';
    return {
        fetchRessources : function(){
            return $http.get(URL);
        },
        fetchHistoryRessources : function(){
            return $http.get(URL+'/history');
        },
        saveRessource : function(ressource){
            return $http.post(URL,ressource);
        },
        deleteRessource : function(ressource){
            return $http.delete(URL+'/'+ressource.id);
        },
        updateRessource : function(ressource){
            return $http.put(URL,ressource);
        }
    }
});
app.factory('ChargesSvc',function($http){
    const URL ='api/charges';
    return {
        fetchCharges : function(){
            return $http.get(URL);
        },
        fetchHistoryCharges : function(){
            return $http.get(URL+'/history');
        },
        saveCharge : function(charge){
            return $http.post(URL,charge);
        },
        deleteCharge : function(charge){
            return $http.delete(URL+'/'+charge.id);
        },
        updateCharge : function(charge){
            return $http.put(URL,charge);
        }
    }
});
app.factory('ModeGestionRessourcesSvc',function($http){
    const URL ='api/modesGestionRessources';
    return {
        fetchModesGestionRessources : function(){
            return $http.get(URL);
        },
        fetchHistoryModesGestionRessources : function(){
            return $http.get(URL+'/history');
        },
        saveModesGestionRessources : function(modeGestionRessources){
            return $http.post(URL,modeGestionRessources);
        },
        deleteModesGestionRessources : function(modeGestionRessources){
            return $http.delete(URL+'/'+modeGestionRessources.id);
        },
        updateModesGestionRessources : function(modeGestionRessources){
            return $http.put(URL, modeGestionRessources);
        }
    }
});
app.factory('ModePaiementTiersSvc',function($http){
    const URL ='api/modesPaiement';
    return {
        fetchModesPaiement : function(){
            return $http.get(URL);
        },
        fetchHistoryModesPaiement : function(){
            return $http.get(URL+'/history');
        },
        saveModesPaiement : function(modePaiement){
            return $http.post(URL,modePaiement);
        },
        deleteModesPaiement : function(modePaiement){
            return $http.delete(URL+'/'+modePaiement.id);
        },
        updateModesPaiement : function(modePaiement){
            return $http.put(URL, modePaiement);
        }
    }
});
app.factory('TypeTiersPayeurSvc',function($http){
    const URL ='api/typeTiersPayeur';
    return {
        fetchDatas : function(){
            return $http.get(URL);
        },
        fetchHistoryDatas : function(){
            return $http.get(URL+'/history');
        },
        saveData : function(data){
            return $http.post(URL,data);
        },
        deleteData : function(data){
            return $http.delete(URL+'/'+data.id);
        },
        updateData : function(data){
            return $http.put(URL, data);
        }
    }
});
app.factory('FamillesArticlesSvc',function($http){
    const URL ='api/famillesArticles';
    return {
        fetchDatas : function(){
            return $http.get(URL);
        },
        fetchHistoryDatas : function(){
            return $http.get(URL+'/history');
        },
        saveData : function(data){
            return $http.post(URL,data);
        },
        deleteData : function(data){
            return $http.delete(URL+'/'+data.id);
        },
        updateData : function(data){
            return $http.put(URL, data);
        }
    }
});
app.factory('ArticlesSvc',function($http){
    const URL ='api/articlesFacturation';
    return {
        fetchDatas : function(){
            return $http.get(URL);
        },
        fetchHistoryDatas : function(){
            return $http.get(URL+'/history');
        },
        saveData : function(data){
            return $http.post(URL,data);
        },
        deleteData : function(data){
            return $http.delete(URL+'/'+data.id);
        },
        updateData : function(data){
            return $http.put(URL, data);
        }
    }
});
